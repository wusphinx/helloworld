package com.caocao.docker.example.helloworld;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HelloworldApplication {
    public static void main(String[] args) throws Exception {
        SpringApplication.run(HelloworldApplication.class, args);
    }
}
